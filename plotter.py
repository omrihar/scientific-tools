#!/usr/bin/env python2
# encoding: utf-8

from __future__ import print_function, division

# Standard imports
import numpy as np
import scipy as sp
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

class Plotter:

    """ This class plots functions. """

    def __init__(self, domain):
        """ Initializes the class and saves a domain.

        Args:
            domain (@todo): @todo

        """
        self._domain = domain

    def plot(self, func):
        """ Plots a function.

        Args:
            func (@todo): @todo

        Returns: @todo

        """
        y = func(self._domain)
        plt.plot(self._domain, y)
        plt.show()

if __name__ == '__main__':
    x = np.linspace(0, 2 * np.pi, 100)
    plotter = Plotter(x)
    plotter.plot(np.sin)

